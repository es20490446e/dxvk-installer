```
USAGE
       vulkanizer [option]

OPTIONS
       install (or empty)

       uninstall

EXCEPTIONS
       Won't install if the graphics driver doesn't support Vulkan
